package com.wordgames.play.ghost;

/**
 * Created by calinserbu on 3/18/17.
 */
public enum GameState {
    DETECTED_COMPLETE_WORD,
    HUMAN_TURN,
    MACHINE_TURN,
    ADMIT_DEFEAT,
    CLAIM_WIN,
    CHALLENGE_HUMAN,
    HUMAN_FORFEITS
}
