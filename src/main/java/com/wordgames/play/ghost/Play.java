package com.wordgames.play.ghost;

import com.wordgames.dictionary.Words;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by calinserbu on 3/21/17.
 */
public class Play {

    private Rules game = null;
    private Scanner scanner = new Scanner(System.in);
    private Words gameWords = null;

    public Play(Words gameWords) throws IOException {
        this.gameWords = gameWords;
        if (this.gameWords.getDictionary() == null) {
            throw new IOException("Unable to load dictionary!");
        }
    }

    public Rules determineFirstTurn() {
        System.out.println("Would you like to have the first round? (Y/N):");
        String yesOrNo = scanner.nextLine().toLowerCase();
        if (yesOrNo.matches("[y]")) {
            System.out.println("Great! Please choose a letter:");
            String playersChoice = scanner.nextLine().toLowerCase();
            if (playersChoice.matches("[a-z]")) {
                game = new Rules(playersChoice, gameWords.getDictionary());
                System.out.printf("Great choice! So we're starting with: %s%n", game.getWord().toString());
            } else {
                System.out.println("Let's use the English alphabet please. Try again....");
            }
        } else if (yesOrNo.matches("[n]")) {
            System.out.println("OK, I'll start then!");
            game = new Rules(gameWords.getDictionary());
            System.out.printf("I pick: %s%n", game.getWord().toString());
        } else {
            System.out.println("I am sorry but I don't understand that answer :-(");
        }
        return game;
    }

    public void playGhost() {
        boolean quit = false;
        if (game != null) {
            //Start taking turns
            while (!quit) {
                switch (game.getState()) {
                    case MACHINE_TURN:
                        game.playMachineTurn();
                        System.out.printf("I pick: %s%n", game.getMyChoice());
                        System.out.printf("So far, we have: %s%n", game.getWord().toString());
                        break;
                    case HUMAN_TURN:
                        System.out.println("Your turn! Pick a letter, or type \"challenge\":");
                        String playersChoice = scanner.nextLine().toLowerCase();
                        if (playersChoice.matches("[a-z]")) {
                            game.playHumanTurn(playersChoice);
                            System.out.printf("So far, we have: %s%n", game.getWord().toString());
                        } else if (playersChoice.matches("^challenge$")) {
                            game.answerChallenge();
                        } else {
                            game.playHumanTurn(playersChoice);
                        }
                        break;
                    case CLAIM_WIN:
                        System.out.printf("I was thinking of: %s%n", game.getChallengeAnswer());
                        System.out.printf("I win! Don't beat yourself up, you're only human. Give it another try!");
                        quit = true;
                        break;
                    case ADMIT_DEFEAT:
                        System.out.println("Congratulations! You win! For now.....");
                        quit = true;
                        break;
                    case CHALLENGE_HUMAN:
                        System.out.println("Hmmmm....I am sure you have an amazing word in mind. Can you share it with me?");
                        String playersWord = scanner.nextLine();
                        game.checkChallengeAnswer(playersWord);
                        break;
                    case DETECTED_COMPLETE_WORD:
                        System.out.printf("Sorry to interrupt, but you just completed a word: %s. I win this round!%n", game.getWord().toString());
                        quit = true;
                        break;
                    case HUMAN_FORFEITS:
                        System.out.println("I knew you were bluffing! I win!");
                        quit = true;
                        break;
                    default:
                        quit = true;
                        break;
                }
            }
        }
    }

}
