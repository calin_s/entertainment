package com.wordgames.play.ghost;

import com.wordgames.dictionary.Trie;
import com.wordgames.dictionary.TrieNode;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by calinserbu on 3/18/17.
 */
public class Rules {

    private TrieNode gamePlayDictionaryProgress = null;
    private StringBuilder word = new StringBuilder();
    private boolean machineStarted = false;
    private Trie gameDictionary = null;
    private String challengeAnswer = null;
    private GameState state = null;
    private String myChoice = null;

    public Rules(String startWithThisLetter, Trie dictionary) {
        gameDictionary = dictionary;
        int characterIndex = startWithThisLetter.toLowerCase().charAt(0);
        gamePlayDictionaryProgress = dictionary.getRoot().getLetters()[characterIndex];
        word.append(startWithThisLetter);
        setState(GameState.MACHINE_TURN);
    }

    public Rules(Trie dictionary) {
        gameDictionary = dictionary;
        String startWithThisLetter = generateRandomLetter();
        machineStarted = true;
        int characterIndex = startWithThisLetter.toLowerCase().charAt(0);
        gamePlayDictionaryProgress = dictionary.getRoot().getLetters()[characterIndex];
        word.append(startWithThisLetter);
        setState(GameState.HUMAN_TURN);
    }

    public String getMyChoice() {
        return myChoice;
    }

    private void setMyChoice(String myChoice) {
        this.myChoice = myChoice;
    }

    public GameState getState() {
        return state;
    }

    private void setState(GameState state) {
        this.state = state;
    }

    public String getChallengeAnswer() {
        return challengeAnswer;
    }

    public StringBuilder getWord() {
        return word;
    }

    public void setWord(StringBuilder word) {
        this.word = word;
    }

    public void setGamePlayDictionaryProgress(TrieNode gamePlayDictionaryProgress) {
        this.gamePlayDictionaryProgress = gamePlayDictionaryProgress;
    }

    /**
     * Capture the letter picked by the human player, check that human player is not bluffing,
     * check that human player did not complete a word, check that human player provided a letter that is part of a valid dictionary entry.
     * Finally, add the letter to our working word and pass control to the machine player.
     *
     * @param proposedLetter
     */
    public void playHumanTurn(String proposedLetter) {
        if (proposedLetter == null || proposedLetter.isEmpty()) {
            setState(GameState.HUMAN_FORFEITS);
        } else if (gamePlayDictionaryProgress.getLetters()[proposedLetter.charAt(0)] == null) {
            word.append(proposedLetter);
            setState(GameState.CHALLENGE_HUMAN);
        } else {
            word.append(proposedLetter);
            gamePlayDictionaryProgress = gamePlayDictionaryProgress.getLetters()[proposedLetter.charAt(0)];
            if (gamePlayDictionaryProgress.isEndOfWord()) {
                setState(GameState.DETECTED_COMPLETE_WORD);
            } else {
                setState(GameState.MACHINE_TURN);
            }
        }
    }

    /**
     * Should human player challenge machine player, machine will randomly pick one of the possible words that derive from our working word.
     * It will do so in order to hide the optimal winning word from the human player.
     */
    public void answerChallenge() {
        List<String> allWordDerivatives = gameDictionary.getAllWordsStartingWith(word, gamePlayDictionaryProgress);
        if (allWordDerivatives.isEmpty()) {
            setState(GameState.ADMIT_DEFEAT);
        } else {
            int totalDerivativesCount = allWordDerivatives.size() > 1 ? allWordDerivatives.size() : 1;
            int randomChoice = new Random().nextInt(totalDerivativesCount);
            this.challengeAnswer = allWordDerivatives.get(randomChoice);
            setState(GameState.CLAIM_WIN);
        }
    }

    /**
     * This method would check that human answered challenge honestly. At the moment, it implicitly trusts the human.
     *
     * @param proposedAnswer
     */
    public void checkChallengeAnswer(String proposedAnswer) {
        // Human Admits Bluffing
        if (proposedAnswer == null || proposedAnswer.isEmpty()) {
            setState(GameState.HUMAN_FORFEITS);
        }
        //Implicitly trust human
        else {
            setState(GameState.ADMIT_DEFEAT);
        }
    }

    /**
     * During its turn, the machine will check all the possible words that it can form based on the current working word.
     * Of those words, it will pick the one that needs the least letters for completion. The total number of letters in the word
     * that the machine chooses will be odd if the machine started the game and even if the human player started the game.
     */
    public void playMachineTurn() {
        List<String> allWordDerivatives = gameDictionary.getAllWordsStartingWith(word, gamePlayDictionaryProgress);

        //Play to my advantage
        if (!allWordDerivatives.isEmpty()) {
            String myWord;
            if (machineStarted) {
                myWord = pickMachineWord(word, allWordDerivatives, true);
            } else {
                myWord = pickMachineWord(word, allWordDerivatives, false);
            }
            if (myWord == null) {
                setState(GameState.ADMIT_DEFEAT);
            } else {
                String myChoice = myWord.substring(word.length(), word.length() + 1);
                word.append(myChoice);
                this.setMyChoice(myChoice);
                this.advanceGamePlayDictionaryProgress(myChoice);
                if (gamePlayDictionaryProgress.isEndOfWord()) {
                    setState(GameState.ADMIT_DEFEAT);
                } else {
                    setState(GameState.HUMAN_TURN);
                }
            }
        } else {
            setState(GameState.ADMIT_DEFEAT);
        }
    }

    private String pickMachineWord(StringBuilder word, List<String> playCandidates, boolean even) {

        Comparator<String> leastLetterToComplete = Comparator.comparingInt(s -> (s.length() - word.length()));
        Predicate<String> evenLength = s -> s.length() % 2 == 0;
        Predicate<String> oddLength = s -> s.length() % 2 != 0;
        Predicate<String> longEnough = s -> (s.length() - word.length()) > 0;

        List<String> myEvenWordChoice = playCandidates.stream()
                .filter(evenLength)
                .filter(longEnough)
                .sorted(leastLetterToComplete)
                .collect(Collectors.toList());
        List<String> myOddWordChoice = playCandidates.stream()
                .filter(oddLength)
                .filter(longEnough)
                .sorted(leastLetterToComplete)
                .collect(Collectors.toList());
        String myWordChoice = null;
        if (even) {
            myWordChoice = findWinningWord(myEvenWordChoice, myOddWordChoice);
        } else {
            myWordChoice = findWinningWord(myOddWordChoice, myEvenWordChoice);
        }
        if (myWordChoice == null) {
            Comparator<String> mostLetterToComplete = Comparator.comparingInt(s -> (word.length() - s.length()));
            Optional<String> lastResort = playCandidates.stream().sorted(mostLetterToComplete).findFirst();
            myWordChoice = lastResort.isPresent() ? lastResort.get() : null;
        }
        return myWordChoice;
    }

    /**
     * Ensures that the first winning word choice does not contain any loosing words. Should that be the case, the next winning word
     * will be picked instead.
     *
     * @param winningWords
     * @param loosingWords
     * @return
     */
    private String findWinningWord(List<String> winningWords, List<String> loosingWords) {
        String wordChoice = null;
        for (String winningWord : winningWords) {
            wordChoice = winningWord;
            for (String loosingWord : loosingWords) {
                if (winningWord.startsWith(loosingWord) ||
                        loosingWord.length() > winningWord.length()) {
                    wordChoice = null;
                    break;
                }
            }
            if (wordChoice != null) {
                break;
            }
        }
        return wordChoice;
    }


    private void advanceGamePlayDictionaryProgress(String myPick) {
        int maxLetters = gamePlayDictionaryProgress.getLetters().length;
        for (int i = 0; i < maxLetters; i++) {
            if (i == myPick.charAt(0)) {
                this.gamePlayDictionaryProgress = gamePlayDictionaryProgress.getLetters()[i];
                break;
            }
        }
    }

    private String generateRandomLetter() {
        Random random = new Random();
        char randomChar = (char) (random.nextInt(26) + 'a');
        return Character.toString(randomChar);
    }
}
