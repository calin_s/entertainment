package com.wordgames.dictionary;

/**
 * Created by calinserbu on 3/17/17.
 */
public class TrieNode {
    private TrieNode[] letters;

    private boolean endOfWord;


    public TrieNode() {
        this.letters = new TrieNode[128];//ASCII char set
    }

    public TrieNode[] getLetters() {
        return letters;
    }

    public boolean isEndOfWord() {
        return endOfWord;
    }

    public void setEndOfWord() {
        this.endOfWord = true;
    }
}
