package com.wordgames.dictionary;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by calinserbu on 3/17/17.
 */
public class Trie {

    private TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    public TrieNode getRoot() {
        return root;
    }

    public void insertWord(String word) {
        TrieNode pointingTo = root;
        word = word.trim();
        for (int i = 0; i < word.length(); i++) {
            int letterIndex = word.charAt(i);
            if (pointingTo.getLetters()[letterIndex] == null) {
                TrieNode newTrieNode = new TrieNode();
                pointingTo.getLetters()[letterIndex] = newTrieNode;
                pointingTo = newTrieNode;
            } else {
                pointingTo = pointingTo.getLetters()[letterIndex];
            }
        }
        pointingTo.setEndOfWord();
    }

    public List<String> getAllWordsStartingWith(StringBuilder prefix, TrieNode lastNodeOfPrefix) {
        List<String> words = new ArrayList<>();
        if (lastNodeOfPrefix != null) {
            if (lastNodeOfPrefix.isEndOfWord()) {
                words.add(prefix.toString());
            }
            getAllDerivedWords(prefix, words, lastNodeOfPrefix);
        }
        return words;
    }

    private void getAllDerivedWords(StringBuilder prefix, List<String> words, TrieNode currentTrieNode) {
        StringBuilder derivedWord = new StringBuilder();
        TrieNode[] nextPossibleLetters = currentTrieNode.getLetters();
        for (int i = 0; i < nextPossibleLetters.length; i++) {
            if (nextPossibleLetters[i] != null) {
                derivedWord.append(prefix).append(Character.toString((char) i));
                if (nextPossibleLetters[i].isEndOfWord()) {
                    words.add(derivedWord.toString());
                }
                getAllDerivedWords(derivedWord, words, nextPossibleLetters[i]);
                derivedWord.delete(0, derivedWord.length());
            }
        }
    }

}
