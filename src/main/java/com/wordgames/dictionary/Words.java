package com.wordgames.dictionary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Created by calinserbu on 3/17/17.
 */
public class Words {

    private final static Charset ENCODING = StandardCharsets.US_ASCII;
    private final static String DICTIONARY_FILE_NAME = "/dictionary.txt";
    private final Trie dictionary;

    public Words() throws IOException {
        dictionary = loadCorpusFromFlatFile();
    }

    public Trie getDictionary() {
        return dictionary;
    }

    private Trie loadCorpusFromFlatFile() throws IOException {
        try (InputStream dictionaryStream = getClass().getResourceAsStream(DICTIONARY_FILE_NAME)) {
            Trie dictionary = new Trie();
            BufferedReader reader = new BufferedReader(new InputStreamReader(dictionaryStream));
            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.matches("^[A-Z].*")) {
                    dictionary.insertWord(line.toLowerCase().trim());
                }
            }
            return dictionary;
        }
    }
}
