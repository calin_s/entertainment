package com.wordgames;

import com.wordgames.dictionary.Words;
import com.wordgames.play.ghost.Play;

import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {
        Words gameWords = new Words();
        Play gamePlay = new Play(gameWords);
        gamePlay.determineFirstTurn();
        gamePlay.playGhost();
    }
}