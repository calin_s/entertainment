package com.wordgames;

import com.wordgames.dictionary.Trie;
import com.wordgames.dictionary.TrieNode;
import com.wordgames.dictionary.Words;
import com.wordgames.play.ghost.GameState;
import com.wordgames.play.ghost.Rules;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by calinserbu on 3/18/17.
 */
public class TestGhostPlay {

    private Words dictionaryContext = null;
    private TrieNode playFromThisNode = null;
    private TrieNode negativePlayFromThisNode = null;
    private final String testWord = "zymos";
    private final String negativeTestWord = "zzz";
    private Trie dictionary;

    private Rules machinesGame = null;
    private Rules playersGame = null;
    private Rules negativeMachinesGame = null;

    @Before
    public void setUp() {
        try {
            dictionaryContext = new Words();
            dictionary = dictionaryContext.getDictionary();
            final TrieNode root = dictionary.getRoot();
            playFromThisNode = getLastTrieNodeOfWord(testWord, root);
            negativePlayFromThisNode = getLastTrieNodeOfWord(negativeTestWord, root);

            //Game that I start
            machinesGame = new Rules(dictionary);
            machinesGame.setWord(new StringBuilder(testWord));
            machinesGame.setGamePlayDictionaryProgress(getLastTrieNodeOfWord(testWord, root));

            //Game started by player
            playersGame = new Rules("a", dictionary);
            playersGame.setWord(new StringBuilder(testWord));
            playersGame.setGamePlayDictionaryProgress(getLastTrieNodeOfWord(testWord, root));

            //Negative game
            negativeMachinesGame = new Rules(dictionary);
            negativeMachinesGame.setWord(new StringBuilder(negativeTestWord));
            negativeMachinesGame.setGamePlayDictionaryProgress(getLastTrieNodeOfWord(negativeTestWord, root));
        } catch (IOException e) {
            System.err.print("FAILED TO READ DICTIONARY FILE!!");
        }
    }

    @Test
    public void testFindAllDerivatives() {
        List<String> derivativeWords = dictionary.getAllWordsStartingWith(new StringBuilder(testWord), playFromThisNode);
        assertThat(derivativeWords.size() == 5, is(true));
    }

    @Test
    public void testFindNoDerivatives() {
        List<String> derivativeWords = dictionary.getAllWordsStartingWith(new StringBuilder(negativeTestWord), negativePlayFromThisNode);
        assertThat(derivativeWords.size() == 0, is(true));
    }

    @Test
    public void testPickWord() {
        machinesGame.playMachineTurn();
        String nextLetterOdd = machinesGame.getWord().substring(machinesGame.getWord().length() - 1);
        assertThat(nextLetterOdd.equalsIgnoreCase("i"), is(true));
    }

    @Test
    public void testUnableToPickWord() {
        negativeMachinesGame.playMachineTurn();
        assertThat(negativeMachinesGame.getState(), is(GameState.ADMIT_DEFEAT));
    }

    private TrieNode getLastTrieNodeOfWord(String searchTerm, TrieNode root) {
        TrieNode pointingTo = root;
        for (int i = 0; i < searchTerm.length(); i++) {
            int letterIndex = searchTerm.charAt(i);
            if (pointingTo.getLetters()[letterIndex] != null) {
                pointingTo = pointingTo.getLetters()[letterIndex];
            } else {
                return null;
            }
        }
        if (pointingTo == root) {
            return null;
        }
        return pointingTo;
    }

}
